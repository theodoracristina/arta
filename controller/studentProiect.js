const studentProiectService = require("./../service/studentProiect")

const getStudentProiect = async (req, res, next) => {
	try {
		const studentProiect = await studentProiectService.get()
		res.status(200).send(studentProiect)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createStudentProiect = async (req, res, next) => {
	const studentProiect = req.body

	if (studentProiect.idStudent && studentProiect.idProiect) {
		await studentProiectService.create(studentProiect)
		res.status(201).send({
			message: "Student proiect created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	getStudentProiect,
	createStudentProiect
}
