const echipaService = require("./../service/echipa")

const getEchipa = async (req, res, next) => {
	// try {
		const echipa = await echipaService.getUsingId(req.params.id)
	// 	res.status(200).send(echipa)
	// } catch (err) {
	// 	res.status(500).send({
	// 		message: `Error occured: ${err.message}`
	// 	})
	// }
}

const getEchipe = async (req, res, next) => {
	try {
		const echipe = await echipaService.get()
		res.status(200).send(echipe)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createEchipa = async (req, res, next) => {
	const echipa = req.body

	if (echipa.denumire) {
		await echipaService.create(echipa)
		res.status(201).send({
			message: "Echipa created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

module.exports = {
	getEchipa,
	createEchipa,
	getEchipe
}
