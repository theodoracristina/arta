const studentService = require("./../service/student")

const getStudent = async (req, res, next) => {
	try {
		const student = await studentService.getUsingEmail(req.params.email)
		res.status(200).send(student)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}
module.exports = {
	getStudent
}
