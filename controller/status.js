const statusService = require("./../service/status")

const getStatus = async (req, res, next) => {
	try {
		const status = await statusService.get()
		res.status(200).send(status)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getStatus
}
