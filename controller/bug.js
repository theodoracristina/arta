const bugService = require("./../service/bug")

const getBugs = async (req, res, next) => {
	try {
		const bugs = await bugService.getUsingIdProiect(req.params.idProiect)
		res.status(200).send(bugs)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createBug = async (req, res, next) => {
	const bug = req.body

	if (
		bug.severitate &&
		bug.prioritate &&
		bug.descriere &&
		bug.link &&
		bug.idProiect &&
		bug.idStatus
	) {
		await bugService.create(bug)
		res.status(201).send({
			message: "Bug created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

const changeBug = async (req, res, next) => {
	const bug = req.body

	if (bug && req.params.idBug) {
		await bugService.change(req.params.idBug, bug)
		res.status(201).send({
			message: "Bug changed"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}
module.exports = {
	getBugs,
	createBug,
	changeBug
}
