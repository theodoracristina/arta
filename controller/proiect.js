const proiectService = require("./../service/proiect")

const getProiect = async (req, res, next) => {
	try {
		const proiect = await proiectService.getById(req.params.id)
		res.status(200).send(proiect)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const getProiecte = async (req, res, next) => {
	try {
		const proiecte = await proiectService.get()
		res.status(200).send(proiecte)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createProiect = async (req, res, next) => {
	const proiect = req.body

	if (proiect.denumire && proiect.repo && proiect.idEchipa) {
		await proiectService.create(proiect)
		res.status(201).send({
			message: "Proiect created"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}

const changeProiect = async (req, res, next) => {
	const proiect = req.body

	if (proiect && req.params.idProiect) {
		await proiectService.change(req.params.idProiect, proiect)
		res.status(201).send({
			message: "Proiect changed"
		})
	} else {
		res.status(400).send({
			message: "Wrong input."
		})
	}
}
module.exports = {
	getProiecte,
	createProiect,
	changeProiect,
	getProiect
}
