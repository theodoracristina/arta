const { Proiect } = require("../models/proiect")

const proiect = {
	get: async () => {
		return await Proiect.findAll().then(proiecte => {
			return proiecte
		})
	},

	getById: async id => {
		return await Proiect.findOne({
			where: {
				id: id
			}
		}).then(proiect => {
			return proiect
		})
	},
	create: async proiect => {
		try {
			return await Proiect.create(proiect)
		} catch (err) {
			throw new Error(err.message)
		}
	},
	change: async (proiectId, proiect) => {
		try {
			return Proiect.update(proiect, { where: { id: parseInt(proiectId) } })
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = proiect
