const { Bug } = require("../models/bug")

const bug = {
	getUsingIdProiect: async idProiect => {
		return await Bug.findAll({
			where: { idProiect: parseInt(idProiect) }
		}).then(bugs => {
			return bugs
		})
	},
	create: async bug => {
		try {
			return await Bug.create(bug)
		} catch (err) {
			throw new Error(err.message)
		}
	},
	change: async (bugId, bug) => {
		try {
			return Bug.update(bug, { where: { id: parseInt(bugId) } })
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = bug
