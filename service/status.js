const { Status } = require("../models/status")

const status = {
	get: async () => {
		return await Status.findAll().then(status => {
			return status
		})
	}
}

module.exports = status
