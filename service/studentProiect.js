const { StudentProiect } = require("../models/studentProiect")

const studentProiect = {
	get: async () => {
		return await StudentProiect.findAll().then(studentProiect => {
			return studentProiect
		})
	},
	create: async studentProiect => {
		try {
			return await StudentProiect.create(studentProiect)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = studentProiect
