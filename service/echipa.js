const { Echipa } = require("../models/echipa")

const echipa = {
	getUsingId: async id => {
		return await Echipa.findOne({
			where: { id: parseInt(id) }
		}).then(echipa => {
			return echipa
		})
	},
	get: async () => {
		return await Echipa.findAll().then(echipe => {
			return echipe
		})
	},
	create: async echipa => {
		try {
			return await Echipa.create(echipa)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = echipa
