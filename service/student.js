const { Student } = require("../models/student")

const student = {
	getUsingEmail: async email => {
		return await Student.findOne({
			where: { email: email }
		}).then(student => {
			return student
		})
	},
	create: async student => {
		try {
			return await Student.create(student)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = student
