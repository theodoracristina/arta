import React from "react"
import axios from "axios"

class AdaugaBug extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			id: "",
			severitate: "",
			prioritate: "",
			descriere: "",
			link: ""
		}
		this.schimbare = this.schimbare.bind(this)
		this.trimite = this.trimite.bind(this)
	}
	schimbare(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async trimite() {
		const payload = {
			severitate: this.state.severitate,
			prioritate: this.state.prioritate,
			descriere: this.state.descriere,
			idProiect: parseInt(this.state.id),
			link: this.state.link,
			idStatus: 1,
			idEchipa: this.props.student.idEchipa
		}
		await axios.post("http://localhost:8080/api/bug", payload)
		this.props.history.push("/detaliiProiect/" + this.state.id)
	}
	async componentDidMount() {
		let id = this.props.match.params.id
		await this.setState({ id: id })
	}

	render() {
		return (
			<div className='col-sm-5 mt-3 ml-3'>
				<form>
					<label>Severitate</label>
					<input
						value={this.state.severitate}
						className='form-control mb-1'
						type='text'
						name='severitate'
						onChange={this.schimbare}
					/>
					<label>Prioritate</label>
					<input
						value={this.state.prioritate}
						className='form-control mb-1'
						type='text'
						name='prioritate'
						onChange={this.schimbare}
					/>
					<label>Descriere</label>
					<input
						value={this.state.descriere}
						className='form-control mb-1'
						type='text'
						name='descriere'
						onChange={this.schimbare}
					/>
					<label>Link</label>
					<input
						value={this.state.link}
						className='form-control mb-1'
						type='text'
						name='link'
						onChange={this.schimbare}
					/>
					<button type='button' className='mt-2 btn btn-xs btn-success' onClick={this.trimite}>
						Trimite
					</button>
				</form>
			</div>
		)
	}
}

export default AdaugaBug
