import React from "react"

class Inregistrare extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			email: "",
			parola: ""
		}

		this.schimbare = this.schimbare.bind(this)
		this.trimite = this.trimite.bind(this)
	}

	schimbare(eveniment) {
		this.setState({
			[eveniment.target.name]: eveniment.target.value
		})
	}

	trimite() {
		let inregistrat = this.props.trimite(this.state.email, this.state.parola)
		inregistrat.then(response => {
			if (response) {
				this.props.history.push("/acasa")
			}
		})
	}

	render() {
		return (
			<div className='mt-2 ml-2 col-sm-5 col-md-5'>
				<h2>Bun venit!</h2>
				<h3>Aceasta este platforma de bug tracking. Introdu datele tale!</h3>
				<div>
					<form>
						<input
							value={this.state.email}
							placeholder='email'
							className='form-control'
							type='text'
							name='email'
							onChange={this.schimbare}
						/>
						<input
							value={this.state.parola}
							placeholder='parola'
							className='form-control'
							type='password'
							name='parola'
							onChange={this.schimbare}
						/>
						<button
							type='button'
							className='btn mt-3 btn-xs btn-success pull-right'
							onClick={this.trimite}
						>
							Trimite
						</button>
					</form>
				</div>
			</div>
		)
	}
}

export default Inregistrare
