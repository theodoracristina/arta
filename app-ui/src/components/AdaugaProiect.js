import React from "react"
import axios from "axios"
import { Link } from "react-router-dom"

class AdaugaProiect extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			denumire: "",
			repo: ""
		}
		this.schimbare = this.schimbare.bind(this)
		this.trimite = this.trimite.bind(this)
	}
	schimbare(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async trimite() {
		const payload = {
			denumire: this.state.denumire,
			repo: this.state.repo,
			idEchipa: this.props.student.idEchipa
		}
		await axios.post("http://localhost:8080/api/proiect", payload)
		this.props.history.push("/acasa")
	}
	render() {
		return (
			<div className='col-sm-5 mt-3 ml-3'>
				<form>
					<label>Denumire</label>
					<input
						value={this.state.denumire}
						className='form-control mb-1'
						type='text'
						name='denumire'
						onChange={this.schimbare}
					/>
					<label>Repository</label>
					<input
						value={this.state.repo}
						className='form-control mb-1'
						type='text'
						name='repo'
						onChange={this.schimbare}
					/>
					<Link to='/acasa'>
						<button class='btn btn-xs btn-danger mr-2 mt-2'>Inapoi</button>
					</Link>
					<button type='button' className='mt-2 btn btn-xs btn-success' onClick={this.trimite}>
						Trimite
					</button>
				</form>
			</div>
		)
	}
}

export default AdaugaProiect
