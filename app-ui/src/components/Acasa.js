import React from "react"
import axios from "axios"
import { Link } from "react-router-dom"

import TabelProiecte from "./TabelProiecte"

class Acasa extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			alteProiecte: [],
			proiectePersonale: [],
			echipe: [],
			studentProiect: []
		}
		this.getStudentProiecte = this.getStudentProiecte.bind(this)
		this.getProiecte = this.getProiecte.bind(this)
	}

	async getStudentProiecte() {
		await axios.get("http://localhost:8080/api/studentProiect").then(studentProiect => {
			this.setState({ studentProiect: studentProiect.data })
		})
	}

	async getProiecte() {
		await axios.get("http://localhost:8080/api/proiecte").then(proiecte => {
			let personale = [],
				altele = []
			proiecte.data.forEach(proiect => {
				if (proiect.idEchipa === this.props.student.idEchipa) {
					personale.push(proiect)
				} else {
					altele.push(proiect)
				}
			})
			this.setState({ proiectePersonale: personale, alteProiecte: altele })
		})
	}

	async componentDidMount() {
		this.getStudentProiecte()
		await axios.get("http://localhost:8080/api/echipe").then(echipe => {
			this.setState({ echipe: echipe.data })
		})
		this.getProiecte()
	}
	render() {
		return (
			<div className='mt-3 ml-3'>
				<Link to='/adaugaProiect'>
					<button className='btn btn-xs btn-success'>Adauga proiect</button>
				</Link>
				<h2 className='mb-2'>Tabel Proiecte personale</h2>
				<TabelProiecte
					membru={true}
					getProiecte={this.getProiecte}
					student={this.props.student}
					echipe={this.state.echipe}
					proiecte={this.state.proiectePersonale}
				></TabelProiecte>
				<h2 className='mb-2'>Tabel Alte proiecte</h2>
				<TabelProiecte
					membru={false}
					student={this.props.student}
					echipe={this.state.echipe}
					proiecte={this.state.alteProiecte}
					studentProiect={this.state.studentProiect}
					getStudentProiecte={this.getStudentProiecte}
				></TabelProiecte>
			</div>
		)
	}
}

export default Acasa
