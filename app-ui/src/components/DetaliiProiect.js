import React from "react"

import axios from "axios"
import TabelBugs from "./TabelBugs"
import { Link } from "react-router-dom"

class DetaliiProiect extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			id: undefined,
			proiect: {},
			bugs: [],
			status: []
		}
		this.getProiect = this.getProiect.bind(this)
		this.getBugs = this.getBugs.bind(this)
	}

	getProiect() {
		axios.get("http://localhost:8080/api/proiect/" + this.state.id).then(proiect => {
			this.setState({ proiect: proiect.data })
		})
	}

	getBugs() {
		axios.get("http://localhost:8080/api/bugs/" + this.state.id).then(bugs => {
			this.setState({ bugs: bugs.data })
		})
	}

	async componentDidMount() {
		await this.setState({ id: this.props.match.params.id })

		this.getProiect()
		await axios.get("http://localhost:8080/api/status").then(status => {
			this.setState({ status: status.data })
		})
		this.getBugs()
	}
	render() {
		return (
			<>
				<h2 className='mt-2 mb-4'>Lista bug-uri</h2>
				<TabelBugs
					student={this.props.student}
					proiect={this.state.proiect}
					bugs={this.state.bugs}
					history={this.props.history}
					getBugs={this.getBugs}
					status={this.state.status}
					membru={this.state.proiect.idEchipa === this.props.student.idEchipa}
				></TabelBugs>
				<br></br>
				<Link to={"/adaugaBug/" + this.state.proiect.id}>
					<button className='btn btn-xs btn-success ml-2'>Adauga</button>
				</Link>
			</>
		)
	}
}

export default DetaliiProiect
