import React from "react"
import axios from "axios"
class RezolvaBug extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			id: "",
			link: "",
			idStatus: "",
			status: []
		}
		this.schimbare = this.schimbare.bind(this)
		this.trimite = this.trimite.bind(this)
	}
	schimbare(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async trimite() {
		const payload = {
			link: this.state.link,
			idStatus: this.state.idStatus
		}
		await axios.put("http://localhost:8080/api/bug/" + this.state.id, payload)
		this.props.history.push("/acasa")
	}

	async componentDidMount() {
		let id = this.props.match.params.id
		await this.setState({ id: id })
		await axios.get("http://localhost:8080/api/status").then(status => {
			status.data.shift()
			this.setState({ status: status.data })
		})
	}

	render() {
		return (
			<div className='col-sm-5 mt-3 ml-3'>
				<form>
					<label>Link</label>
					<input
						value={this.state.link}
						className='form-control mb-1'
						type='text'
						name='link'
						onChange={this.schimbare}
					/>
					<label>Prioritate</label>
					<select
						value={this.state.idStatus}
						className='form-control'
						name='idStatus'
						onChange={this.schimbare}
					>
						{this.state.status.map(status => (
							<option value={status.id}>{status.denumire}</option>
						))}
					</select>
					<button type='button' className='mt-2 btn btn-xs btn-success' onClick={this.trimite}>
						Trimite
					</button>
				</form>
			</div>
		)
	}
}

export default RezolvaBug
