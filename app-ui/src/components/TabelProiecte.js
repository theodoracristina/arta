import React from "react"
import { Link } from "react-router-dom"
import axios from "axios"

class TabelProiecte extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			editMode: false,
			denumire: "",
			repo: "",
			proiectId: ""
		}

		this.getEchipa = this.getEchipa.bind(this)
		this.addTester = this.addTester.bind(this)
		this.isTester = this.isTester.bind(this)
		this.startEdit = this.startEdit.bind(this)
		this.stopEdit = this.stopEdit.bind(this)
		this.modificaProiect = this.modificaProiect.bind(this)
		this.schimbare = this.schimbare.bind(this)
	}

	schimbare(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}
	async modificaProiect(idProiect) {
		await axios.put("http://localhost:8080/api/proiect/" + idProiect, {
			denumire: this.state.denumire,
			repo: this.state.repo
		})
		this.stopEdit()
		this.props.getProiecte()
	}
	getEchipa(id) {
		return this.props.echipe.find(echipa => echipa.id === id).denumire || undefined
	}
	startEdit(proiect) {
		this.setState({
			editMode: true,
			denumire: proiect.denumire,
			repo: proiect.repo,
			proiectId: proiect.id
		})
	}
	stopEdit() {
		this.setState({ editMode: false, denumire: "", repo: "", proiectId: "" })
	}
	isTester(idProiect) {
		return (
			this.props.studentProiect &&
			this.props.studentProiect.find(
				studentProiect =>
					studentProiect.idStudent === this.props.student.id &&
					studentProiect.idProiect === idProiect
			)
		)
	}

	async addTester(idProiect) {
		await axios.post("http://localhost:8080/api/studentProiect", {
			idStudent: this.props.student.id,
			idProiect: idProiect
		})

		this.props.getStudentProiecte()
	}

	render() {
		return (
			<table className='table'>
				<thead>
					<tr>
						<td>Denumire</td>
						<td>Repository</td>
						<td>Echipa</td>
						{this.props.membru ? (
							<>
								<td>Detalii</td>
								<td>Edit</td>
							</>
						) : (
							<td>Actiune</td>
						)}
					</tr>
				</thead>
				<tbody>
					{this.props.proiecte.map(proiect => (
						<tr>
							{this.state.editMode && this.state.proiectId === proiect.id ? (
								<>
									<td>
										<input
											value={this.state.denumire}
											className='form-control'
											type='text'
											name='denumire'
											onChange={this.schimbare}
										/>
									</td>
									<td>
										<input
											value={this.state.repo}
											className='form-control'
											type='text'
											name='repo'
											onChange={this.schimbare}
										/>
									</td>
									<td>{this.getEchipa(proiect.idEchipa)}</td>
									<>
										<td>
											<button className='btn btn-xs btn-danger' onClick={this.stopEdit}>
												Cancel
											</button>
										</td>
										<td>
											<button
												className='btn btn-xs btn-success'
												onClick={() => this.modificaProiect(proiect.id)}
											>
												Trimite
											</button>
										</td>
									</>
								</>
							) : (
								<>
									<td>{proiect.denumire}</td>
									<td>{proiect.repo}</td>
									<td>{this.getEchipa(proiect.idEchipa)}</td>
									{this.props.membru ? (
										<>
											<td>
												<Link to={"/detaliiProiect/" + proiect.id}>
													<button className='btn btn-xs btn-primary'>Detalii</button>
												</Link>
											</td>
											<td>
												<button
													className='btn btn-xs btn-success'
													onClick={() => this.startEdit(proiect)}
												>
													Edit
												</button>
											</td>
										</>
									) : this.isTester(proiect.id) ? (
										<td>
											<Link to={"/detaliiProiect/" + proiect.id}>
												<button className='btn btn-xs btn-primary'>Detalii</button>
											</Link>
										</td>
									) : (
										<td>
											<button
												className='btn btn-xs btn-primary'
												onClick={() => this.addTester(proiect.id)}
											>
												Tester
											</button>
										</td>
									)}
								</>
							)}
						</tr>
					))}
				</tbody>
			</table>
		)
	}
}

export default TabelProiecte
