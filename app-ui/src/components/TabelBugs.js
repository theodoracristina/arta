import React from "react"
import axios from "axios"

class TabelBugs extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}

		this.esteAsignataLaMine = this.esteAsignataLaMine.bind(this)
		this.esteAsignata = this.esteAsignata.bind(this)
		this.asigneaza = this.asigneaza.bind(this)
		this.rezolva = this.rezolva.bind(this)
		this.getStatus = this.getStatus.bind(this)
	}
	esteAsignataLaMine(idBug) {
		return this.props.bugs.find(
			bug => bug.id === idBug && bug.idResponsabil === this.props.student.id
		)
	}
	esteAsignata(idBug) {
		return this.props.bugs.find(bug => bug.id === idBug && bug.idResponsabil)
	}
	getStatus(idStatus) {
		return this.props.status.find(status => status.id === idStatus).denumire
	}
	async asigneaza(idBug) {
		await axios.put("http://localhost:8080/api/bug/" + idBug, {
			idResponsabil: this.props.student.id
		})

		this.props.getBugs()
	}
	rezolva(idBug) {
		this.props.history.push("/rezolvaBug/" + idBug)
	}

	render() {
		return (
			<table className='table'>
				<thead>
					<tr>
						<td>Severitate</td>
						<td>Prioritate</td>
						<td>Descriere</td>
						<td>Status</td>
						<td>Link</td>
						{this.props.membru ? <td>Actiune</td> : null}
					</tr>
				</thead>
				<tbody>
					{this.props.bugs.map(bug => (
						<tr>
							<td>{bug.severitate}</td>
							<td>{bug.prioritate}</td>
							<td>{bug.descriere}</td>
							<td>{this.getStatus(bug.idStatus)}</td>
							<td>{bug.link}</td>
							{this.props.membru ? (
								this.esteAsignataLaMine(bug.id) ? (
									bug.idStatus !== 1 ? (
										<td>
											<p>Bug inchis</p>
										</td>
									) : (
										<td>
											<button
												className='btn btn-xs btn-success'
												onClick={() => this.rezolva(bug.id)}
											>
												Rezolva
											</button>
										</td>
									)
								) : this.esteAsignata(bug.id) ? (
									<td>
										<p>Deja asignata</p>
									</td>
								) : (
									<td>
										<button
											className='btn btn-xs btn-success'
											onClick={() => this.asigneaza(bug.id)}
										>
											Asigneaza
										</button>
									</td>
								)
							) : null}
						</tr>
					))}
				</tbody>
			</table>
		)
	}
}

export default TabelBugs
