import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Inregistrare from "./components/Inregistrare"
import Acasa from "./components/Acasa"
import DetaliiProiect from "./components/DetaliiProiect"

import axios from "axios"
import AdaugaProiect from "./components/AdaugaProiect"
import RezolvaBug from "./components/RezolvaBug"
import AdaugaBug from "./components/AdaugaBug"

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			student: {}
		}

		this.trimite = this.trimite.bind(this)
	}

	async trimite(email, parola) {
		return await axios.get(`http://localhost:8080/api/student/${email}`).then(response => {
			if (response.data.parola === parola) {
				this.setState({ student: response.data })
				return true
			} else {
				return false
			}
		})
	}
	render() {
		return (
			<div>
				<Router>
					<Switch>
						<Route
							exact
							path={"/"}
							render={props => (
								<Inregistrare history={props.history} trimite={this.trimite}></Inregistrare>
							)}
						/>
						<Route
							exact
							path={"/acasa"}
							render={props => <Acasa history={props.history} student={this.state.student}></Acasa>}
						/>
						<Route
							exact
							path={"/adaugaProiect"}
							render={props => (
								<AdaugaProiect history={props.history} student={this.state.student}></AdaugaProiect>
							)}
						/>
						<Route
							exact
							path={"/adaugaBug/:id"}
							render={props => (
								<AdaugaBug
									history={props.history}
									student={this.state.student}
									{...props}
								></AdaugaBug>
							)}
						/>
						<Route
							exact
							path={"/rezolvaBug/:id"}
							render={props => (
								<RezolvaBug
									history={props.history}
									student={this.state.student}
									{...props}
								></RezolvaBug>
							)}
						/>
						<Route
							exact
							path={"/detaliiProiect/:id"}
							render={props => (
								<DetaliiProiect
									history={props.history}
									student={this.state.student}
									{...props}
								></DetaliiProiect>
							)}
						/>
					</Switch>
				</Router>
			</div>
		)
	}
}

export default App
