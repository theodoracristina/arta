const configuration = require("../config/configuration.json")
const Sequelize = require("sequelize")

const DB_NAME = configuration.database.database_name
const DB_USER = configuration.database.username
const DB_PASS = configuration.database.password

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	dialect: "mysql"
})

sequelize
	.authenticate()
	.then(() => {
		console.log("Database connection success!")
	})
	.catch(err => {
		console.log(`Database connection error: ${err}`)
	})

class Bug extends Sequelize.Model {}

Bug.init(
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		severitate: {
			type: Sequelize.STRING,
			allowNull: false
		},
		prioritate: {
			type: Sequelize.STRING,
			allowNull: false
		},
		descriere: {
			type: Sequelize.STRING,
			allowNull: false
		},
		idStatus: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		idResponsabil: {
			type: Sequelize.INTEGER,
			allowNull: true
		},
		idProiect: {
			type: Sequelize.INTEGER,
			allowNull: true
		},
		link: {
			type: Sequelize.STRING,
			allowNull: true
		}
	},
	{
		sequelize,
		modelName: "bugs"
	}
)

// Bug.sync({ force: true })

module.exports = {
	sequelize,
	Bug
}
